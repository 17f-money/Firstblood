---
title: Smart Contract
subtitle: The Brains Behind the Operation
date: 2017-11-05
---

Firstblood works around a smart contract on the Ethereum blockchain. This smart contract functions as a means to handle rewards and matchmaking within Firstblood. Sending a stake of 1ST to the contract address initiates matchmaking, allowing you to be matched with an opposing player or team with a similar bet. The smart contract handles all consensus on how much is rewarded after the game is played.

```javascript
contract Token {

/// @return total amount of tokens
function totalSupply() constant returns (uint256 supply) {}

/// @param _owner The address from which the balance will be retrieved
/// @return The balance
function balanceOf(address _owner) constant returns (uint256 balance) {}

/// @notice send `_value` token to `_to` from `msg.sender`
/// @param _to The address of the recipient
/// @param _value The amount of token to be transferred
/// @return Whether the transfer was successful or not
function transfer(address _to, uint256 _value) returns (bool success) {}

/// @notice send `_value` token to `_to` from `_from` on the condition it is approved by `_from`
/// @param _from The address of the sender
/// @param _to The address of the recipient
/// @param _value The amount of token to be transferred
/// @return Whether the transfer was successful or not
function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {}

/// @notice `msg.sender` approves `_addr` to spend `_value` tokens
/// @param _spender The address of the account able to transfer the tokens
/// @param _value The amount of wei to be approved for transfer
/// @return Whether the approval was successful or not
function approve(address _spender, uint256 _value) returns (bool success) {}

/// @param _owner The address of the account owning tokens
/// @param _spender The address of the account able to transfer the tokens
/// @return Amount of remaining tokens allowed to spent
function allowance(address _owner, address _spender) constant returns (uint256 remaining) {}

event Transfer(address indexed _from, address indexed _to, uint256 _value);
event Approval(address indexed _owner, address indexed _spender, uint256 _value);

}

```


The above code is a segment of the Firstblood smart contract which handles the initial betting system. It checks if both players are eligible to play, meaning that they are in ownership of the correct amount of 1ST tokens.

The smart contract controls countless other functions, including manipulation of a player's rating; such as through the User Reputation System and Match Making Rating. The player's Global Rating is also determined by the smart contract after matches are played and verified.