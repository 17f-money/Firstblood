---
title: First Tokens
date: 2017-11-05
---

Firstblood had its ICO in September 2016, opening sales for First Tokens (1ST). During the token pre-sale 1 ETH was equivalent to about 170 1ST. The rate decreased to 1 ETH for 150 1ST after the first "Power Hour," continuing to decrease linearly until it reached a standard of 1 ETH for 100 1ST. The token sale raised  465,312 ETH (~ 5.5 million USD). The token sale smart contract sets the cap equivalent to this value.


1ST are the main app tokens which power the Firstblood Ecosystem. By inputting 1 ETH to the smart contract address, users are given 100 1ST. This 1ST can be used as stake in matches between other players. The token has many uses within Firstblood. Users can transfer 1ST within the web application and may obtain it as a reward by being a member of the Jury Voting Pool or by acting as a Witness.

Firstblood has a referral system where players can invite other users to join the platform. Those who refer new players are rewarded with 1ST to bet in matches.

