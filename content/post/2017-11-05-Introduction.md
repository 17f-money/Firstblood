---
title: What is Firstblood?
subtitle: A New Way to Play
date: 2017-11-05
---

Firstblood is a competitive e-sports platform based around the Ethereum blockchain. Users have the ability to earn money by playing 1 v 1 or team vs team matches in several online games such as Dota 2, League of Legends, and Counter-Strike Global Offensive. 

Firstblood is fully autonomous, allowing bets and transactions to be carried out without the use of a middleman. The P2P (peer-to-peer) decentralization enables players to earn without worrying about relying on others to process payments and without risking losing their money to organizational corruption. The system is reliant on its Witnesses and JVP (Jury Voting Pool) to fight back against fradulent reports by accurately assessing the quality of games and ensuring the correct amount of money is delivered to those who successfully earned it.

The system is powered by an Ethereum smart-contract which handles all matchmaking and rewards. By sending a stake of 1ST Token to the contract (Firstblood's primary currency), users are able to begin competing against other players. Other than handling bets and transactions between players, the Firstblood smart contract also controls the Global Ranking and Match Making Rating of its players. These features ensure that users of comparable skill are matched against one another. Outside of these features, the User Reputation System and the Dynamic Rewards System function as a means to control payout as users progress and the platform grows.

